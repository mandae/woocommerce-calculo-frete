/* global wp, ajaxurl, WCMandaeAdminOrdersParams */
jQuery(function ($) {

    /**
     * Admin class.
     *
     * @type {Object}
     */
    var WCMandaeAdminOrders = {

        /**
         * Initialize actions.
         */
        init: function () {
            $(document.body)
                .on('click', '.mandae-tracking-code .dashicons-dismiss', this.removeTrackingCode)
                .on('click', '.mandae-tracking-code .button-secondary', this.addTrackingCode);
        },

        /**
         * Block meta boxes.
         */
        block: function () {
            $('#wc_mandae').block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });
        },

        /**
         * Unblock meta boxes.
         */
        unblock: function () {
            $('#wc_mandae').unblock();
        },

        /**
         * Add tracking fields.
         *
         * @param {Object} $el Current element.
         */
        addTrackingFields: function (trackingCodes) {
            var $wrap = $('body #wc_mandae .mandae-tracking-code');
            var template = wp.template('tracking-code-list');

            $('.mandae-tracking-code__list', $wrap).remove();
            $wrap.prepend(template({'trackingCodes': trackingCodes}));
        },

        /**
         * Add tracking code.
         *
         * @param {Object} evt Current event.
         */
        addTrackingCode: function (evt) {
            evt.preventDefault();

            var $el = $('#mandae-add-tracking-code');
            var trackingCode = $el.val();

            if ('' === trackingCode) {
                return;
            }

            var self = WCMandaeAdminOrders;
            var data = {
                action: 'woocommerce_mandae_add_tracking_code',
                security: WCMandaeAdminOrdersParams.nonces.add,
                order_id: WCMandaeAdminOrdersParams.order_id,
                tracking_code: trackingCode
            };

            self.block();

            // Clean input.
            $el.val('');

            // Add tracking code.
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function (response) {
                    self.addTrackingFields(response.data);
                    self.unblock();
                }
            });
        },

        /**
         * Remove tracking fields.
         *
         * @param {Object} $el Current element.
         */
        removeTrackingFields: function ($el) {
            var $wrap = $('body #wc_mandae .mandae-tracking-code__list');

            if (1 === $('li', $wrap).length) {
                $wrap.remove();
            } else {
                $el.closest('li').remove();
            }
        },

        /**
         * Remove tracking code.
         *
         * @param {Object} evt Current event.
         */
        removeTrackingCode: function (evt) {
            evt.preventDefault();

            // Ask if really want remove the tracking code.
            if (!window.confirm(WCMandaeAdminOrdersParams.i18n.removeQuestion)) {
                return;
            }

            var self = WCMandaeAdminOrders;
            var $el = $(this);
            var data = {
                action: 'woocommerce_mandae_remove_tracking_code',
                security: WCMandaeAdminOrdersParams.nonces.remove,
                order_id: WCMandaeAdminOrdersParams.order_id,
                tracking_code: $el.data('code')
            };

            self.block();

            // Remove tracking code.
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function () {
                    self.removeTrackingFields($el);
                    self.unblock();
                }
            });
        }
    };

    WCMandaeAdminOrders.init();
});
