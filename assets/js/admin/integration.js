/* global ajaxurl, WCMandaeIntegrationAdminParams */
jQuery(function ($) {

    /**
     * Admin class.
     *
     * @type {Object}
     */
    var WCMandaeIntegrationAdmin = {

        /**
         * Initialize actions.
         */
        init: function () {
            $(document.body).on('click', '#woocommerce_mandae-integration_autofill_empty_database', this.empty_database);
        },

        /**
         * Empty database.
         *
         * @return {String}
         */
        empty_database: function () {
            if (!window.confirm(WCMandaeIntegrationAdminParams.i18n_confirm_message)) {
                return;
            }

            $('#mainform').block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'mandae_autofill_addresses_empty_database',
                    nonce: WCMandaeIntegrationAdminParams.empty_database_nonce
                },
                success: function (response) {
                    window.alert(response.data.message);
                    $('#mainform').unblock();
                }
            });
        }
    };

    WCMandaeIntegrationAdmin.init();
});
