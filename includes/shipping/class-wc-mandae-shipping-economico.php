<?php
/**
 * Mandae PAC shipping method.
 *
 * @package WooCommerce_Mandae/Classes/Shipping
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * PAC shipping method class.
 */
class WC_Mandae_Shipping_Economico extends WC_Mandae_Shipping
{
    /**
     * Initialize PAC.
     *
     * @param int $instance_id Shipping zone instance.
     */
    public function __construct($instance_id = 0)
    {
        $this->id           = 'mandae-economico';
        $this->method_title = __('Mandaê - Econômico', 'woocommerce-mandae');
        $this->method_code = 'Econômico';

        parent::__construct($instance_id);
    }
}
