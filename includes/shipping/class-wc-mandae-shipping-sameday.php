<?php
/**
 * Mandae PAC shipping method.
 *
 * @package WooCommerce_Mandae/Classes/Shipping
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Same-day shipping method class.
 */
class WC_Mandae_Shipping_Sameday extends WC_Mandae_Shipping
{
    /**
     * Initialize Same-day.
     *
     * @param int $instance_id Shipping zone instance.
     */
    public function __construct($instance_id = 0)
    {
        $this->id           = 'mandae-sameday';
        $this->method_title = __('Mandaê - Same Day', 'woocommerce-mandae');
        $this->method_code = 'Same-Day';

        parent::__construct($instance_id);
    }
}
