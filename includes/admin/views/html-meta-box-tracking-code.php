<?php
/**
 * Meta box - Tracking Code
 *
 * @package WooCommerce_Mandae/Admin/Settings
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<div class="mandae-tracking-code">
    <?php if (!empty($tracking_codes)) : ?>
        <div class="mandae-tracking-code__list">
            <strong><?php echo esc_html(_n('Tracking code:', 'Tracking codes:', count($tracking_codes), 'woocommerce-mandae')); ?></strong>
            <ul>
                <?php foreach ($tracking_codes as $tracking_code) : ?>
                    <li>
                        <span aria-label="<?php esc_attr_e('Tracking code', 'woocommerce-mandae') ?>"><?php echo esc_html($tracking_code); ?></span>
                        <a href="#" class="dashicons-dismiss"
                           title="<?php esc_attr_e('Remove tracking code', 'woocommerce-mandae'); ?>"
                           aria-label="<?php esc_attr_e('Remove tracking code', 'woocommerce-mandae') ?>"
                           data-code="<?php echo esc_attr($tracking_code); ?>"></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <fieldset>
        <label for="mandae-add-tracking-code"><?php esc_html_e('Add tracking code', 'woocommerce-mandae'); ?></label>
        <input type="text" id="mandae-add-tracking-code" name="mandae_tracking" value=""/>
        <button type="button" class="button-secondary dashicons-plus"
                aria-label="<?php esc_attr_e('Add new tracking code', 'woocommerce-mandae'); ?>"></button>
    </fieldset>
</div>

<script type="text/html" id="tmpl-tracking-code-list">
    <div class="mandae-tracking-code__list">
        <# if ( 1
        <
        data.trackingCodes.length ) { #>
        <strong><?php esc_html_e('Tracking codes:', 'woocommerce-mandae'); ?></strong>
        <# } else { #>
            <strong><?php esc_html_e('Tracking code:', 'woocommerce-mandae'); ?></strong>
            <# } #>
                <ul>
                    <# _.each( data.trackingCodes, function( trackingCode ) { #>
                        <li><span aria-label="<?php esc_attr_e('Tracking code', 'woocommerce-mandae') ?>">{{trackingCode}}</span>
                            <a href="#" class="dashicons-dismiss"
                               title="<?php esc_attr_e('Remove tracking code', 'woocommerce-mandae') ?>"
                               aria-label="<?php esc_attr_e('Remove tracking code', 'woocommerce-mandae') ?>"
                               data-code="{{trackingCode}}"></a></li>
                        <# }); #>
                </ul>
    </div>
</script>
