<?php
/**
 * List table - Tracking Code
 *
 * @package WooCommerce_Mandae/Admin/Settings
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<div class="mandae-tracking-code">
    <small class="meta">
        <?php echo esc_html(_n('Tracking code:', 'Tracking codes:', count($tracking_codes), 'woocommerce-mandae')); ?>
        <?php echo implode(' | ', $tracking_codes); ?>
    </small>
</div>
