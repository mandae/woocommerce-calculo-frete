<?php
/**
 * Mandae Webservice.
 *
 * @package WooCommerce_Mandae/Classes/Webservice
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

class Request{

    /**
     * array containing the products.
     *
     * @var array
     */
    public $items = null;

     /**
     * applyInsurance Signals that the declared value will be used in the freight calculation
     *
     * @var string
     */
    public $applyInsurance = null;
}        

/**
 * Mandae Webservice integration class.
 */
class WC_Mandae_Webservice
{
    const SANDBOX_URL = 'https://sandbox.api.mandae.com.br/';
 
    const LIVE_URL = 'https://api.mandae.com.br/';

    /**
     * Shipping method ID.
     *
     * @var string
     */
    protected $id = '';

    /**
     * Shipping zone instance ID.
     *
     * @var int
     */
    protected $instance_id = 0;

    /**
     * Authorization.
     *
     * @var string
     */
    protected $auth = '';

    /**
     * Authorization.
     *
     * @var string
     */
    protected $environment = '';

    /**
     * WooCommerce package containing the products.
     *
     * @var array
     */
    protected $package = null;

    /**
     * Destination postcode.
     *
     * @var string
     */
    protected $destination_postcode = '';

    /**
     * Debug mode.
     *
     * @var string
     */
    protected $debug = 'no';

    /**
     * Logger.
     *
     * @var WC_Logger
     */
    public $log = null;

    /**
     * Whether to use declared value or not
     *
     * @var bool
     */
    private $use_declared_value = false;  
    
    /**
     * Initialize webservice.
     *
     * @param string $id Method ID.
     * @param int $instance_id Instance ID.
     */
    public function __construct($id = 'mandae', $instance_id = 0)
    {        
        $this->id          = $id;
        $this->instance_id = $instance_id;
        $this->log         = new WC_Logger();
        $this->use_declared_value = get_option('woocommerce_mandae-integration_settings', $default = false )['declared_value'];
    }

    /**
     * Set auth.
     *
     * @param string $auth Authorization.
     */
    public function set_auth($auth = '')
    {
        $this->auth = $auth;
    }

    /**
     * Set environment.
     *
     * @param string $environment Environment.
     */
    public function set_environment($environment = '')
    {
        $this->environment = $environment;
    }

    /**
     * Set shipping package.
     *
     * @param array $package Shipping package.
     */
    public function set_package($package = array())
    {
        $this->package = $package;
    }

    /**
     * Set destination postcode.
     *
     * @param string $postcode Destination postcode.
     */
    public function set_destination_postcode($postcode = '')
    {
        $this->destination_postcode = $postcode;
    }

    /**
     * Set the debug mode.
     *
     * @param string $debug Yes or no.
     */
    public function set_debug($debug = 'no')
    {
        $this->debug = $debug;
    }

    /**
     * Get authorization.
     *
     * @return string
     */
    public function get_auth()
    {
        return apply_filters('woocommerce_mandae_auth', $this->auth, $this->id, $this->instance_id, $this->package);
    }

    /**
     * Get environment.
     *
     * @return string
     */
    public function get_environment()
    {
        return apply_filters('woocommerce_mandae_environment', $this->environment, $this->id, $this->instance_id, $this->package);
    }

    /**
     * @param $environment
     * @return string
     */
    private function get_url($environment)
    {
        if ($environment == 'sandbox') {
            return self::SANDBOX_URL;
        }

        return self::LIVE_URL;
    }

    /**
     * Get shipping prices.
     *
     * @return SimpleXMLElement|array
     */
    public function get_shipping()
    {
        $url = $this->get_url($this->environment);

        $url .= 'v4/postalcodes/' . str_replace("-", "", $this->destination_postcode) . '/rates';

        $items = array_map(function ($item) {
            $product = $item['data'];

            $_height = max(1, wc_get_dimension((float)$product->get_height(), 'cm'));
            $_width  = max(1, wc_get_dimension((float)$product->get_width(), 'cm'));
            $_length = max(1, wc_get_dimension((float)$product->get_length(), 'cm'));
            $_weight = max(0.000001, wc_get_weight((float)$product->get_weight(), 'kg'));

            $itemData = [
                'quantity' => $item['quantity'],
                'weight' => $_weight,
                'height' => $_height,
                'width' => $_width,
                'length' => $_length,
                'declaredValue' => $product->get_price(),
            ];           
            return $itemData;
        }, array_values($this->package['contents']));

        $itemData = [
            'quantity' => 1,
            'weight' => 0,
            'height' => 0,
            'width' => 0,
            'length' => 0,
            'declaredValue' => 0,
        ];

        $items2 = [];
        $limiteAltura = 105;

        foreach ($items as $item) {
            if ($item['height'] > $limiteAltura)
                throw new \Exception('Item excede o limite de altura.');

            $restantes = $item['quantity'];

            while ($restantes > 0) {
                $capacidade = floor(105 / $item['height']);
                $itemData2 = $itemData;
                $itemData2['weight'] += $item['weight'];
                $itemData2['width'] += $item['width'];
                $itemData2['length'] += $item['length'];
                $itemData2['height'] += $item['height'];
                $itemData2['quantity'] = min($capacidade, $restantes);
                $itemData2['declaredValue'] += $item['declaredValue'];     

                $items2[] = $itemData2;
                $restantes -= $capacidade;
            }                  
        }
       
        $request = new Request();        
        $request->items = $items2;   
        $request->applyInsurance = $this->use_declared_value ? "true" : "false";

        $body = json_encode($request);

        $this->log->add($this->id, 'BODY: ' . $body);
        $shipping = null;

        try {
            $response = wp_remote_post(
                $url,
                array(
                    'headers' => array(
                        'Content-Type'  => 'application/json',
                        'Authorization' => $this->auth
                    ),
                    'body' => $body
                )
            );

            $this->log->add($this->id, json_encode($response));

            if (!is_wp_error($response)) {
                if(strstr($response['body'], '{"message":"Too Many Requests"}')){
                    sleep(1);
                    $shipping = $response = wp_remote_post(
                        $url,
                        array(
                            'headers' => array(
                                'Content-Type'  => 'application/json',
                                'Authorization' => $this->auth
                            ),
                            'body' => $body
                        )
                    );                  
                 }else{
                    $shipping = $response['body'];
                 }
            }
        } catch (Exception $e) {
            $this->log->add($this->id, $e->getMessage());
            return null;
        }

        return $shipping;
    }  
}