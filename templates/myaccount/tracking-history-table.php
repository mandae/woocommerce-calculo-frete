<?php
/**
 * Tracking history table.
 *
 * @author  BizCommerce
 * @package WooCommerce_Mandae/Templates
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<p class="wc-mandae-tracking__description"><?php esc_html_e('History for the tracking code:', 'woocommerce-mandae'); ?>
    <strong><?php echo esc_html($trackingCode); ?></strong></p>
<p class="wc-mandae-tracking__description"><?php esc_html_e('Carrier Code:', 'woocommerce-mandae'); ?>
    <strong><?php echo esc_html($carrierCode); ?></strong></p>
<p class="wc-mandae-tracking__description"><?php esc_html_e('Carrier Name:', 'woocommerce-mandae'); ?>
    <strong><?php echo esc_html($carrierName); ?></strong></p>

<table class="wc-mandae-tracking__table woocommerce-table shop_table shop_table_responsive">
    <thead>
    <tr>
        <th style="width: 20%"><?php esc_html_e('Date', 'woocommerce-mandae'); ?></th>
        <th><?php esc_html_e('Name', 'woocommerce-mandae'); ?></th>
        <th><?php esc_html_e('Description', 'woocommerce-mandae'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($events as $event) : ?>
        <tr>
            <td><?php echo esc_html($event->date); ?></td>
            <td>
                <?php echo esc_html($event->name); ?>
            </td>
            <td>
                <?php echo esc_html($event->description); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>