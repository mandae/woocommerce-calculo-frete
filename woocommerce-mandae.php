<?php
/**
 * Plugin Name:          WooCommerce Mandaê
 * Plugin URI:           https://www.mandae.com.br/
 * Description:          Adds Mandae shipping methods to your WooCommerce store.
 * Author:               Mandaê
 * Author URI:           https://www.mandae.com.br/
 * Version:              1.2.5
 * License:              GPLv2 or later
 * Text Domain:          woocommerce-mandae
 * Domain Path:          /languages
 * WC requires at least: 3.0.0
 *
 * WooCommerce Mandae is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * WooCommerce Mandae is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WooCommerce Mandae. If not, see
 * <https://www.gnu.org/licenses/gpl-2.0.txt>.
 *
 * @package WooCommerce_Mandae
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('WC_Mandae')) {

    /**
     * WooCommerce Mandae main class.
     */
    class WC_Mandae
    {

        /**
         * Plugin version.
         *
         * @var string
         */
        const VERSION = '1.2.5';

        /**
         * Instance of this class.
         *
         * @var object
         */
        protected static $instance = null;

        /**
         * Initialize the plugin public actions.
         */
        private function __construct()
        {
            add_action('init', array($this, 'load_plugin_textdomain'), -1);
            add_action('init', array($this, 'init_form_fields'), -1);
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));

            if (class_exists('WC_Integration')) {
                $this->includes();

                if (is_admin()) {
                    $this->admin_includes();
                }

                add_filter('woocommerce_integrations', array($this, 'include_integrations'));
                add_filter('woocommerce_shipping_methods', array($this, 'include_methods'));
                add_filter('woocommerce_email_classes', array($this, 'include_emails'));
            } else {
                add_action('admin_notices', array($this, 'woocommerce_missing_notice'));
            }
        }

        /**
         * Return an instance of this class.
         *
         * @return object A single instance of this class.
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Load the plugin text domain for translation.
         */
        public function load_plugin_textdomain()
        {
            load_plugin_textdomain('woocommerce-mandae', false, dirname(plugin_basename(__FILE__)) . '/languages/');
        }

        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable', 'mandaee'),
                    'type' => 'checkbox',
                    'description' => __('Enable this shipping.', 'mandaee'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Title', 'mandaee'),
                    'type' => 'text',
                    'description' => __('Title to be display on site', 'mandaee'),
                    'default' => __('Cedran Shipping', 'mandaee')
                ),
                'weight' => array(
                    'title' => __('Weight (kg)', 'mandaee'),
                    'type' => 'number',
                    'description' => __('Maximum allowed weight', 'mandaee'),
                    'default' => 100
                ),
            );
        }

        /**
         * Includes.
         */
        private function includes()
        {
            include_once dirname(__FILE__) . '/includes/wc-mandae-functions.php';
            include_once dirname(__FILE__) . '/includes/class-wc-mandae-install.php';
            include_once dirname(__FILE__) . '/includes/class-wc-mandae-webservice.php';
            include_once dirname(__FILE__) . '/includes/class-wc-mandae-tracking-history.php';

            include_once dirname(__FILE__) . '/includes/integrations/class-wc-mandae-integration.php';

            if (defined('WC_VERSION') && version_compare(WC_VERSION, '2.6.0', '>=')) {
                include_once dirname(__FILE__) . '/includes/abstracts/abstract-wc-mandae-shipping.php';

                foreach (glob(plugin_dir_path(__FILE__) . '/includes/shipping/*.php') as $filename) {
                    include_once $filename;
                }

                WC_Mandae_Install::upgrade_300_from_wc_260();
            }

            WC_Mandae_Install::upgrade_300();
        }


        public function plugin_action_links($links)
        {
            $plugin_links = array();
            $plugin_links[] = '<a href="' . esc_url(admin_url('admin.php?page=wc-settings&tab=integration&section=mandae-integration')) . '">' . __('Configurações', 'woocommerce-mandae') . '</a>';
            $plugin_links[] = '<a href="' . esc_url(admin_url('admin.php?page=wc-settings&tab=shipping')) . '">' . __('Entrega', 'woocommerce-mandae') . '</a>';
            return array_merge($plugin_links, $links);
        }

        /**
         * Admin includes.
         */
        private function admin_includes()
        {
            include_once dirname(__FILE__) . '/includes/admin/class-wc-mandae-admin-orders.php';
        }

        /**
         * Include Mandae integration to WooCommerce.
         *
         * @param  array $integrations Default integrations.
         *
         * @return array
         */
        public function include_integrations($integrations)
        {
            $integrations[] = 'WC_Mandae_Integration';

            return $integrations;
        }

        /**
         * Include Mandae shipping methods to WooCommerce.
         *
         * @param  array $methods Default shipping methods.
         *
         * @return array
         */
        public function include_methods($methods)
        {
            $methods['mandae-legacy'] = 'WC_Mandae_Shipping_Legacy';

            if (defined('WC_VERSION') && version_compare(WC_VERSION, '2.6.0', '>=')) {
                $methods['mandae-expresso'] = 'WC_Mandae_Shipping_Expresso';
                $methods['mandae-economico'] = 'WC_Mandae_Shipping_Economico';
                $methods['mandae-rapido'] = 'WC_Mandae_Shipping_Rapido';
                $methods['mandae-sameday'] = 'WC_Mandae_Shipping_Sameday';

                $old_options = get_option('woocommerce_mandae_settings');

                if (empty($old_options)) {
                    unset($methods['mandae-legacy']);
                }
            }

            return $methods;
        }

        /**
         * Include emails.
         *
         * @param  array $emails Default emails.
         *
         * @return array
         */
        public function include_emails($emails)
        {
            if (!isset($emails['WC_Mandae_Tracking_Email'])) {
                $emails['WC_Mandae_Tracking_Email'] = include(dirname(__FILE__) . '/includes/emails/class-wc-mandae-tracking-email.php');
            }

            return $emails;
        }

        /**
         * WooCommerce fallback notice.
         */
        public function woocommerce_missing_notice()
        {
            include_once dirname(__FILE__) . '/includes/admin/views/html-admin-missing-dependencies.php';
        }

        /**
         * Get main file.
         *
         * @return string
         */
        public static function get_main_file()
        {
            return __FILE__;
        }

        /**
         * Get plugin path.
         *
         * @return string
         */
        public static function get_plugin_path()
        {
            return plugin_dir_path(__FILE__);
        }

        /**
         * Get templates path.
         *
         * @return string
         */
        public static function get_templates_path()
        {
            return self::get_plugin_path() . 'templates/';
        }
    }

    add_action('plugins_loaded', array('WC_Mandae', 'get_instance'));

}
